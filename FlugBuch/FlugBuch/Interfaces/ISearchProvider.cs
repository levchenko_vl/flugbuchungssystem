﻿using FlugBuch.Helpers.Context;
using System.Collections.Generic;

namespace FlugBuch.Interfaces
{
    public interface ISearchProvider<TDomainModel>
    {
        TDomainModel SearchById(int id);
        IEnumerable<TDomainModel> SearchByContext(SearchContextBase searchContext);
    }
}
