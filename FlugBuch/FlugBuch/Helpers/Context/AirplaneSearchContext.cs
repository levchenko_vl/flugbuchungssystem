﻿namespace FlugBuch.Helpers.Context
{
    public class AirplaneSearchContext : SearchContextBase
    {        
        public AirplaneSearchContext(string airplaineSearchString)
        {
            AirplaneNumber = airplaineSearchString;
        }

        public string AirplaneNumber { get; private set; }
    }
}
