﻿using System.ComponentModel.DataAnnotations.Schema;
namespace FlugBuch.DomainModels
{
    public class Airplane
    {
        [Column("AirplaneId")]
        public int Id { get; set; }
        [Column("AirplaneName")]
        public string Name { get; set; }
        //public string Company { get; set; }
    }
}
