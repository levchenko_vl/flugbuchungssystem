﻿using FlugBuch.Interfaces;
using System;
using System.Collections.Generic;
using FlugBuch.Helpers.Context;
using FlugBuch.DomainModels;
using Levl.DAL.Repositories.EFRepository;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FlugBuch.SearchProviders
{
    public class AirplaneSearchProvider : ISearchProvider<Airplane>
    {
        private readonly EntityFrameworkApplicationContext _entityFrameworkApplicationContext;

        public AirplaneSearchProvider (EntityFrameworkApplicationContext entityFrameworkApplicationContext)
        {
            _entityFrameworkApplicationContext = entityFrameworkApplicationContext;
        }

        public IEnumerable<Airplane> SearchByContext(SearchContextBase searchContext)
        {
            var airplaneSearchContext = searchContext as AirplaneSearchContext;
            //_entityFrameworkApplicationContext.Airplanes.Where(airplane => airplane.Name)
            var airplanes =
            from a in _entityFrameworkApplicationContext.Airplanes
            where EF.Functions.Like(a.Name, $"%{airplaneSearchContext.AirplaneNumber}%")
            select a;

            return airplanes.ToList();
        }

        public Airplane SearchById(int id)
        {
            return _entityFrameworkApplicationContext.Find<Airplane>(id);
        }
    }
}
