﻿using FlugBuch.DomainModels;
using Microsoft.EntityFrameworkCore;

namespace Levl.DAL.Repositories.EFRepository
{
    public class EntityFrameworkApplicationContext : DbContext
    {
        public EntityFrameworkApplicationContext(DbContextOptions<EntityFrameworkApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<Airplane> Airplanes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Airplane>()
                .ToTable("Airplane","TestDataBase")
                .HasKey(a => a.Id);
            
        }
    }
}
