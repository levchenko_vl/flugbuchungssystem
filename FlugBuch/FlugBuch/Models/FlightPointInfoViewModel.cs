﻿using System;

namespace FlugBuch.Models
{
    public class FlightPointInfoViewModel
    {
        public DateTime Date { get; set; }
        public int FlightPointId { get; set; }
    }
}
