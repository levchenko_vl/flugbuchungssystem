﻿using System.Collections.Generic;

namespace FlugBuch.Models
{
    public class AirplanesListViewModel
    {
        public IEnumerable<AirplaneViewModel> Airplanes { get; set; }
    }
}
