﻿namespace FlugBuch.Models
{
    public class AirplaneSearchViewModel
    {
        public string AirplaineSearchString { get; set; }

        public FlightPointInfoViewModel  EndPointViewModel{ get; set; }

        public FlightPointInfoViewModel StartPointViewModel { get; set; }

    }
}
