﻿using FlugBuch.Adapters;
using FlugBuch.DomainModels;
using FlugBuch.Helpers.Context;
using FlugBuch.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace FlugBuch.Controllers
{
    public class AirplaneSearchController : Controller
    {
        private readonly AirplaneSearchAdapter _airplainSearchAdapter;
        private readonly ISearchProvider<Airplane> _searchProvider;

        public AirplaneSearchController(AirplaneSearchAdapter airplainSearchAdapter, ISearchProvider<Airplane> searchProvider)
        {
            _airplainSearchAdapter = airplainSearchAdapter;
            _searchProvider = searchProvider;
        }
                
        [HttpGet]
        public IActionResult Index()
        {
            var model = _airplainSearchAdapter.CreateAirplaineViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SearchAirplaneByString(string airplaineSearchString)
        {
            var airplanes = _searchProvider.SearchByContext(new AirplaneSearchContext (airplaineSearchString));
            if (airplanes.Any())
            {
                var airplanesListViewModel = _airplainSearchAdapter.CreateAirplainesListViewModel(airplanes);
                return View("AirplanesList", airplanesListViewModel);
            };

            var model = _airplainSearchAdapter.CreateAirplaineViewModel();
            return View("Index", model);
        }
    }
}