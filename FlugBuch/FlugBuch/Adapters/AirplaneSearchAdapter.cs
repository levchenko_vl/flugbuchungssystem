﻿using FlugBuch.Models;
using System;
using FlugBuch.DomainModels;
using System.Collections.Generic;
using System.Linq;

namespace FlugBuch.Adapters
{
    public class AirplaneSearchAdapter
    {
        public AirplaneSearchViewModel CreateAirplaineViewModel()
        {
            return new AirplaneSearchViewModel();
        }

        internal AirplanesListViewModel CreateAirplainesListViewModel(IEnumerable<Airplane> airplanes)
        {
            var model = new AirplanesListViewModel();
            model.Airplanes = airplanes.Select(airplane =>
            {
                var airp = new AirplaneViewModel();
                airp.AirplaneName = airplane.Name;
                return airp;
            });

            return model;
        }
    }
}
