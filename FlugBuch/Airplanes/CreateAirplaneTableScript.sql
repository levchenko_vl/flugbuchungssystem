﻿CREATE SCHEMA TestDataBase;
GO

CREATE TABLE TestDataBase.Airplane (
    AirplaneId int NOT NULL PRIMARY KEY IDENTITY,
    AirplaneName varchar(255) UNIQUE NOT NULL, 
	INDEX AirplaneId_Index NONCLUSTERED (AirplaneId))