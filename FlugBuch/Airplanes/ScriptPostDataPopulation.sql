﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
PRINT 'STATRT OF DATA POPULATION SCRIPT'

INSERT INTO [TestDataBase].[Airplane]  (AirplaneName)
SELECT SUBSTRING(CONVERT(VARCHAR(255), NEWID()),0,7) from sys.columns


PRINT 'Insert into [TestDataBase].[Airplane] was done'


PRINT 'END OF DATA POPULATION SCRIPT'

